//右键分享相关
function genericOnClick(info, tab) { 
	window.open('https://task.congcong.us/notes?add_content='+info.linkUrl);
} 
function pageOnClick(info, tab) { 
	window.open('https://task.congcong.us/notes?add_content='+info.pageUrl );
} 
function selectionOnClick(info, tab) { 
	window.open('https://task.congcong.us/notes?add_content='+info.selectionText);
} 
function imageOnClick(info, tab) { 
	window.open('https://task.congcong.us/notes?type=image&add_content='+info.srcUrl);
} 
var link = chrome.contextMenus.create({"title": "分享当前链接到Montage GTD","contexts":["link"],"onclick":genericOnClick}); 
var page = chrome.contextMenus.create({"title": "分享当前页面到Montage GTD","contexts":["page"],"onclick":pageOnClick}); 
var image = chrome.contextMenus.create({"title": "分享当前图片到Montage GTD","contexts":["image"],"onclick":imageOnClick}); 
var selection = chrome.contextMenus.create({"title": "分享选中文字到Montage GTD","contexts":["selection"],"onclick":selectionOnClick}); 

//通知相关
function show(title,content) {
  new Notification(title, {
    icon: 'icon-large.ico',
    body: content
  });
}

if (!localStorage.isInitialized) {
  localStorage.isActivated = false;   // The display activation.
  localStorage.isInitialized = true; // The option initialization.
}

// Test for notification support.
if (window.Notification) {
  var interval = 0; // The display interval, in minutes.

  setInterval(function() {
    interval++;
    if (
      JSON.parse(localStorage.isActivated) 
    ) {
      show('测试标题','测试内容');
	  localStorage.isActivated = false; 
      interval = 0;
    }
  }, 60000);
}